"""
INFO:
    -> Plotting functions for read_images.py
AUTHOR:
    -> Samuel Gutierrez R <samuel.gutierrez@ug.uchile.cl>
CREATE:
    -> 02-07-20
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from PIL import Image


def plot_sextractor(img_full_dir, xyz_data_list, save_fig=False):
    """
    Plot stars image with the Source Extractor solution.

    Args:
        img_full_dir (str)  : The full directory of the stars image to plot.
        xyz_data_list (list): The list with position (X, Y) and magnitude (Z) of extracted objects.
        save_fig (bool)     : A boolean to control the option of save the image in current directory.

    Returns:
        int: 0 if successful.
    """
    # Get image in the correct orientation.
    image = Image.open(img_full_dir)
    image_bw = np.flipud(image.convert('L'))
    # Get X and Y values.
    sex_x_pix = [x[0] for x in xyz_data_list]
    sex_y_pix = [x[1] for x in xyz_data_list]
    # Get image name.
    img_full_name = img_full_dir.split('/')
    img_name = "".join([img_full_name[-2], '/', img_full_name[-1]])
    fig, axs = plt.subplots(1, 2, figsize=(16, 10))
    # Plots.
    fig.suptitle('Image name: {}'.format(img_name))
    axs[0].set_title('Original .jpg image')
    axs[0].set_xlabel('Pixel')
    axs[0].set_ylabel('Pixel')
    axs[0].imshow(image_bw, cmap='binary', norm=colors.LogNorm(vmin=0.7, vmax=3))
    axs[0].set_ylim(0, 1024)
    axs[0].set_xlim(0, 1024)
    axs[1].set_title('Original .jpg image + SExtractor result')
    axs[1].set_ylabel('Pixel')
    axs[1].set_xlabel('Pixel')
    axs[1].imshow(image_bw, cmap='binary', norm=colors.LogNorm(vmin=0.7, vmax=3), zorder=0)
    axs[1].scatter(sex_x_pix, sex_y_pix, s=80, facecolors='None', edgecolors='r', linewidths=2, zorder=1)
    axs[1].set_ylim(0, 1024)
    axs[1].set_xlim(0, 1024)
    if save_fig:
        fig.savefig('Img_stars_and_sextractor.png', dpi=300, format='png')
    plt.show()
    return 0
