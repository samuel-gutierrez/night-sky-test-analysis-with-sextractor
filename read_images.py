"""
INFO:
    -> Reading images of night-sky measurements made of David Gonzalez
AUTHOR:
    -> Samuel Gutierrez R <samuel.gutierrez@ug.uchile.cl>
CREATE:
    -> 02-07-20
"""
import utilities
import plotting

DIR_IMG_FOLDER = '/home/samuel/Insync/samuel.gutierrez@usach.cl/Google Drive - Shared drives/Tesis/' \
                 'Pruebas Rpi Cam/Prueba #3 - 8mm - 12mm - Nativa (25-04-2020 _ 10PM-11.10PM)/'
IMG_NAME = 'Nativa/nat_1_26_04_-_02_10_04_time_500.jpg'
DIR_IMG_FULL = "".join([DIR_IMG_FOLDER, IMG_NAME])


def main():
    # Transform jpg to fits.
    utilities.jpg2fits(DIR_IMG_FULL)
    # SExtract fits image and read catalog.
    utilities.apply_sextractor()
    xyz_data = utilities.testcat2xyz()
    # Plot two images.
    plotting.plot_sextractor(DIR_IMG_FULL, xyz_data)


if __name__ == '__main__':
    main()
